import datetime
import time


class BankAccount:

    def __init__(self,first_name:str,last_name:str,id:str,balance:float):
        self.first_name = first_name
        self.last_name = last_name
        self.id=id
        self.balance=balance
        self.comition = 0.1
        self.log = {}

    def withdrawal(self,amount:int):
        if amount<=self.balance:
            self.balance = self.balance-amount*(1+self.comition)
            self.log[self.get_time()]=f"withdrawal {amount} balance {self.balance}"
            return self.balance
        return "not enough balance to withdrawal"

    def deposite(self,amount):
        self.balance = self.balance+amount*(1-self.comition)
        self.log[self.get_time()] = f"deposite {amount} balance {self.balance}"
        return self.balance

    def get_time(self):
        current_datetime = datetime.datetime.now()
        formatted_datetime = current_datetime.strftime('%d-%m-%Y %H:%M:%S.%f')
        return formatted_datetime
    def status(self):

        return self.balance

    def transfer(self,name):
        pass

    def history(self):
        return self.log

    def transfer(self,amount:float,destination_account):
        if amount > self.balance:
            print("not enough money to transfer")
        if self.balance>=amount:
            self.withdrawal(amount)
            destination_account.deposite(amount)

